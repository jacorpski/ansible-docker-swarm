# Intro
This is a project trying out to setup a 5 node Docker Swarm (3 managers, 2 workers) with as much as possible automation. Ansible will be used to setup all the necessary stuff.

The project will be hosted on DigitalOcean, which means the initial server setup will follow the best practice for setting up a server there.

# Ansible
## Installing
`brew install ansible`

This will install Ansible on macOS

## How to use
* Add the IP address you want to control in the hosts file
* Use `ansible -i [hosts] [host-pattern] -m ping -u root` to check if you have connection to the servers
* Use `ansible-playbook -i [hosts] [playbook.yml] [-f 1]` to set the servers

`[hosts]` is the place where the hosts file is placed. If not set in the command, Ansible will use `/etc/ansible/hosts` as default.

`[host-pattern]` tells which pattern to use. If `all` is specified, it will run against all servers in the hosts file. It's possible to run against single by just specify their name in the hosts file, like `node1`

`[playbook.yml]` tells which playbook to you. A playbook is a list of tasks to run. An example is `do_init_server.yml` which will setup server(s) in best practice on DigitalOcean

`[-f 1]` is necessary if you run against multiple new servers that you haven't been connected to through SSH. [It's an know bug](https://github.com/ansible/ansible/issues/25068) in Ansible and the workaround it to number of parallel process to 1. If you have connected to the servers before, `[-f 1]` can be ignored